import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import { SET_PAGE } from '../constants/actionTypes';

const mapDispatchToProps = dispatch => ({
  onSetPage: (page, payload) =>
    dispatch({ type: SET_PAGE, page, payload })
});

const ListPagination = props => {
  if (props.articlesCount <= 10) {
    return null;
  }

  const range = [];
  for (let i = 0; i < Math.ceil(props.articlesCount / 10); ++i) {
    range.push(i);
  }
  let renderedRange = [];
  if(range<10)
  {
    renderedRange = [...range];
  }
  else{
    renderedRange = [...range].slice(props.currentPage,props.currentPage+10);
  }

  const nextRenderedRange = () => {
    //renderedRange = [...range].slice(props.currentPage+1,(props.currentPage+1)+10);
    setPage(props.currentPage+1);
  };
  const previousRenderedRange = () => {
    //renderedRange = [...range].slice(props.currentPage-1,(props.currentPage-1)+10);
    setPage(props.currentPage-1);
  };
  const setPage = page => {
    if (props.pager) {
      props.onSetPage(page, props.pager(page));
    } else {
      props.onSetPage(page, agent.Articles.all(page))
    }
  };

  return (
    <nav>
      <ul className="pagination">
        <li  className={(props.currentPage == 0)?'page-item disabled':'page-item'}>
          <a className="page-link" href="#" aria-label="Previous"  onClick={previousRenderedRange}>
            <span aria-hidden="true">&laquo;</span>
            <span className="sr-only">Previous</span>
          </a>
        </li>
        {
          renderedRange.map(v => {
            const isCurrent = v === props.currentPage;
            const onClick = ev => {
              ev.preventDefault();
              setPage(v);
            };
            return (


              <li
                className={isCurrent ? 'page-item active' : 'page-item'}
                onClick={onClick}
                key={v.toString()}>

                <a className="page-link" href="">{v + 1}</a>

              </li>


            );
          })
        }
        <li className={(props.currentPage == Math.ceil(props.articlesCount / 10)-1)?'page-item disabled':'page-item'}>
          <a className="page-link" href="#" aria-label="Next" onClick={nextRenderedRange}>
            <span aria-hidden="true">&raquo;</span>
            <span className="sr-only">Next</span>
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default connect(() => ({}), mapDispatchToProps)(ListPagination);
