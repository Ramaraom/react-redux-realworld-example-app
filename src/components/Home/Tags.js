import React from 'react';
import agent from '../../agent';

const Tags = props => {
  const tags = props.tags;
  let tagList = []; 
  if (tags) {
    return (
      <div className="tag-list">
        {
          tags.map(tag => {
            const handleClick = ev => {
              ev.preventDefault();
              if (tagList.includes(tag)) {
                var index = tagList.indexOf(tag);

                if (index != -1) { 

                  tagList.splice(index, 1);
                }
                //tagList.push(tag);
              }
              else {
                tagList.push(tag); 
              }
              props.onClickTag(tagList, page => agent.Articles.byTag(tagList.join(","), page), agent.Articles.byTag(tagList.join(",")));
            };

            return (
              <a
                href=""
                className="tag-default tag-pill"
                key={tag}
                onClick={handleClick}>
                {tag}
              </a>
            );
          })
        }
      </div>
    );
  } else {
    return (
      <div>Loading Tags...</div>
    );
  }
};

export default Tags;
